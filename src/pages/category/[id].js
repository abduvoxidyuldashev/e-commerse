import { request } from "@/services/http-client";
import Products from "@/components/UI/Products";
import SEO from "@/components/SEO";

function Category({ products }) {
  // console.log("products", products);
  return (
    <>
      <SEO />
      <Products products={products} />
    </>
  );
}

export async function getServerSideProps(ctx) {
  try {
    // Make a GET request using Axios
    const products = await request.get(
      `/wc/v3/products?per_page=100&category=${ctx.query.id}`
    );

    return {
      props: {
        products: products,
      },
    };
  } catch (error) {
    console.error("Error fetching data:", error);
    return {
      props: {
        data: null, // Pass null or handle error as needed
      },
    };
  }
}

export default Category;
