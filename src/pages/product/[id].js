import { request } from "@/services/http-client";
import SEO from "@/components/SEO";
import SingleProduct from "@/components/UI/SingleProduct";

function Product({ data }) {
  return (
    <>
      <SEO />
      <SingleProduct product={data} />
    </>
  );
}

export async function getServerSideProps(ctx) {
  try {
    // Make a GET request using Axios
    const product = await request.get(`/wc/v3/products/${ctx.query.id}`);

    return {
      props: {
        data: product,
      },
    };
  } catch (error) {
    console.error("Error fetching data:", error);
    return {
      props: {
        data: null, // Pass null or handle error as needed
      },
    };
  }
}

export default Product;
