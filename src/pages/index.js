import SEO from "@/components/SEO";
import { request } from "@/services/http-client";
import Categories from "@/components/UI/Categories";

function Home({ categories }) {
  return (
    <>
      <SEO />
      <Categories categories={categories} />
    </>
  );
}

export async function getServerSideProps() {
  try {
    // Make a GET request using Axios
    // const products = await request.get("/wc/v3/products?per_page=12");
    const categories = await request.get(
      "/wc/v3/products/categories?per_page=50"
    );

    return {
      props: {
        // products: products,
        categories: categories,
      },
    };
  } catch (error) {
    console.error("Error fetching data:", error);
    return {
      props: {
        data: null, // Pass null or handle error as needed
      },
    };
  }
}

export default Home;
