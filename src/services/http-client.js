import axios from "axios";
import { QueryClient } from "react-query";

const API_BASE_URL = "https://joybox.uz/wp-json";

export const request = axios.create({
  baseURL: API_BASE_URL,
  headers: {
    Authorization:
      "Basic Y2tfMzk4MjQxZjU4YjNiOWI1YjMyMTNmNmUzNjQyNWUwODk1NzViNGNlNDpjc181NzZkZjVhODhlNGJjYWY2OTI2OTY5OTIzMmQ4ZmVhZGEzNWZmZTNk",
  },
});

const errorHandler = (error) => {
  console.log("error ===>", error);

  return Promise.reject(error.response);
};

request.interceptors.response.use((response) => response.data, errorHandler);

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      retry: false,
    },
  },
});
