import React from "react";
import { Container } from "@mui/material";
import Image from "next/image";

const SingleProduct = ({ product }) => {
  return (
    <Container className="my-10">
      <div className="max-w-md mx-auto bg-white border rounded-xl shadow overflow-hidden md:max-w-2xl">
        <div className="md:flex">
          <div className="md:flex-shrink-0 m-3">
            {/* <img
              className="h-[250px] w-full object-contain md:w-[250px]"
              src={product?.images[0]?.src || "https://via.placeholder.com/150"}
              alt={product.name}
            /> */}
            <div className="relative p-3">
              <Image
                src={
                  product?.images[0]?.src ||
                  "https://joybox.uz/wp-content/uploads/2022/03/cropped-555.png"
                }
                alt="Description of image"
                objectFit="contain"
                width={250}
                height={300}
                loading="lazy" // Lazy load the image
              />
            </div>
          </div>
          <div className="p-8">
            <div className="uppercase tracking-wide text-sm text-indigo-500 font-semibold">
              {product.categories[0]?.name}
            </div>
            <a
              href="#"
              className="block mt-1 text-lg leading-tight font-medium text-black hover:underline"
            >
              {product.name}
            </a>
            {/* <p className="mt-2 text-gray-500">{product.description}</p> */}
            <div dangerouslySetInnerHTML={{ __html: product.description }} />
            <div className="mt-4">
              <span className="text-gray-500">Price:</span> ${product.price}
            </div>
            <button className="mt-4 bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded">
              Add to Cart
            </button>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default SingleProduct;
