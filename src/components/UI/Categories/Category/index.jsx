import Image from "next/image";
import React from "react";
import Link from "next/link";

const Category = ({ imageUrl, name, id }) => {
  return (
    <div className="w-[220px]  rounded overflow-hidden shadow-[rgba(0,_0,_0,_0.16)_0px_1px_4px] m-4">
      <Link
        href={`/category/${id}` || ""}
        className="block relative cursor-pointer "
      >
        <a>
          <div className="relative h-48 w-48 p-1 ">
            <Image
              alt={name}
              src={
                imageUrl ||
                "https://joybox.uz/wp-content/uploads/2022/03/cropped-555.png"
              }
              layout="fill"
              objectFit="contain"
              className="rounded-t "
            />
          </div>
        </a>
      </Link>
      <div className="px-6 py-4 text-center">
        <div className="font-bold text-xl mb-2">{name}</div>
      </div>
    </div>
  );
};

export default Category;
