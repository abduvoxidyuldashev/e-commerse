import React from "react";
import ProductItem from "./Product";
import { Container } from "@mui/material";
import { Typography } from "@mui/material";

export default function Products({ products }) {
  return (
    <Container className="my-10">
      <Typography className="mt-6" variant="h4" component="h4">
        {products[0]?.categories[0]?.name}
      </Typography>
      <div className="flex flex-wrap justify-center">
        {products.map((product, index) => (
          <ProductItem
            imageUrl={product?.images && product?.images[0]?.src}
            key={index}
            name={product.name}
            id={product.id}
          />
        ))}
      </div>
    </Container>
  );
}
