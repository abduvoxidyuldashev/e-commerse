import Link from "next/link";
import Image from "next/image";

const ProductItem = ({ imageUrl, name, id }) => {
  return (
    <div className="w-[220px]  rounded overflow-hidden shadow-[rgba(0,_0,_0,_0.16)_0px_1px_4px] m-4">
      <Link
        href={`/product/${id}` || ""}
        className="block relative cursor-pointer "
      >
        <a>
          <div className="relative p-3">
            <Image
              src={
                imageUrl ||
                "https://joybox.uz/wp-content/uploads/2022/03/cropped-555.png"
              }
              alt="Description of image"
              objectFit="contain"
              width={300}
              height={400}
              loading="lazy" // Lazy load the image
            />
          </div>
          <div className="px-6 py-4 text-center">
            <div className="font-bold text-xl mb-2">{name}</div>
          </div>
        </a>
      </Link>
    </div>
  );
};

export default ProductItem;
